import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Eap200Decoder {

	public Eap200Decoder(){}
	
	public void decode(File fileConfig, File fileXML) throws IOException{
		int encoByte;
		byte decByte, keyByte = 0;
			FileInputStream fileInputStream = new FileInputStream(fileConfig);
			FileOutputStream fileOutputStream = new FileOutputStream(fileXML);
			fileInputStream.skip(128);
			while ((encoByte = fileInputStream.read()) != -1) {
				decByte = (byte) ~(encoByte - keyByte);
				fileOutputStream.write(new byte[] { decByte });
				keyByte = (byte) ((keyByte + 1) % 64);
			}
			fileInputStream.close();
			fileOutputStream.close();
	}
	
	

	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("No arguments");
			System.err.println("arg1: config file");
			System.err.println("arg2: xml file");
			return;
		}
		File fileConfig = new File(args[0]);
		File fileXML = new File(args[1]);
		try {
			new Eap200Decoder().decode(fileConfig, fileXML);;
			System.out.println("Decrypted file: " + fileXML.getName());
		} catch (IOException e) {
			System.err.println("An error occurred during decryption");
			System.err.println("Error:'"+e.getMessage()+"'");
		}
	}
}
