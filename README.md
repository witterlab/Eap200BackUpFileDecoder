# Eap200BackUpFileDecoder
Decrypts and decodes backup files (.config) from eap200 routers.  
The decoding data is saved in a yml yml file.
## Use
`java Eap200Decoder <test.conf> <test.xml>`
## Download
You can find the last executable file in the [GitLab Artifacts](https://gitlab.com/fabiankranewitter/Eap200BackUpFileDecoder/-/pipelines) of this project.  
Simply select the last pipeline and click on the download button.
